﻿using System.Collections.Generic;
using System.Linq;
using RayApp.Entity;

namespace RayApp.Data
{
    public class DataAccess
    {
        public static List<Product> GetAllProducts()
        {
            using (var ctx = new DataContext())
            {
                return ctx.Products.ToList();
            }
        }


        public static Product GetProduct(string id)
        {
            using (var ctx = new DataContext())
            {
                return ctx.Products.FirstOrDefault(x => x.Name.Contains(id));
            }
        }

        public static void AddProduct(Product product)
        {
            using (var ctx = new DataContext())
            {
                ctx.Products.Add(product);
                ctx.SaveChanges();
            }
        }

        public static void UpdateProduct(Product product)
        {
            using (var ctx = new DataContext())
            {
                var prod = ctx.Products.Find(product.Id);
                if (prod == null) return;
                prod.Name = product.Name;
                prod.Price = prod.Price;
                ctx.SaveChanges();
            }
        }

        public static void DeleteProduct(Product product)
        {
            using (var ctx = new DataContext())
            {
                ctx.Products.Remove(product);
                ctx.SaveChanges();
            }
        }
    }
}
